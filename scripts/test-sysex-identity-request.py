#!/usr/bin/python
import sys
import serial
import time

MIDI_SYSEX = 0xF0
MIDI_SYSEX_TYPE_NON_REALTIME = 0x7E
MIDI_SYSEX_END = 0xF7
MIDI_SYSEX_GENERAL_INFORMATION = 0x06
MIDI_SYSEX_REQUEST_IDENTITY = 0x01
MIDI_SYSEX_REPLY_IDENTITY = 0x02

class MidiError(Exception):
    pass

def sysexIdentityRequest(device):
    serialMidi = serial.Serial(device, 115200, timeout=3)

    def readOneByte():
        return int.from_bytes(serialMidi.read(), 'little', signed=False)

    def cleanBufferFromSysEx():
        while readOneByte() != MIDI_SYSEX_END:
            pass

    def tryParsingSysexIdentityReply():
        sysex_byte = readOneByte()
        if sysex_byte != MIDI_SYSEX:
            print('[info] Received 0x{:02X}, but waiting for MIDI_SYSEX (0x{:02X})...'.format(sysex_byte, MIDI_SYSEX))
            return False

        print('[info] received sysex message...')

        sysex_nonrealtime_type = readOneByte()
        if sysex_nonrealtime_type != MIDI_SYSEX_TYPE_NON_REALTIME:
            print('[err] Received sysex type of 0x{:02X}, but expected MIDI_SYSEX_TYPE_NON_REALTIME (0x{:02X}). Aborting.'.format(sysex_nonrealtime_type, MIDI_SYSEX_TYPE_NON_REALTIME))
            return False

        sysex_channel = readOneByte()
        print('[info] sysex channel is 0x{:02X}'.format(sysex_channel))

        sysex_general_information = readOneByte()
        if sysex_general_information != MIDI_SYSEX_GENERAL_INFORMATION:
            raise MidiError('[err] Received sysex action of 0x{:02X}, but expected MIDI_SYSEX_GENERAL_INFORMATION (0x{:02X}) Aborting.'.format(sysex_general_information, MIDI_SYSEX_GENERAL_INFORMATION))

        sysex_reply_identity = readOneByte()
        if sysex_reply_identity != MIDI_SYSEX_REPLY_IDENTITY:
            raise MidiError('Received sysex reply of 0x{:02X}, but expected MIDI_SYSEX_REPLY_IDENTITY (0x{:02X})'.format(sysex_reply_identity, MIDI_SYSEX_REPLY_IDENTITY))

        manufacturer_id = readOneByte()
        family_code_one = readOneByte()
        family_code_two = readOneByte()
        model_number_one = readOneByte()
        model_number_two = readOneByte()
        version_number_one = readOneByte()
        version_number_two = readOneByte()
        version_number_three = readOneByte()
        version_number_four = readOneByte()

        identity = {
            "manufacturer": "0x{:02x}".format(manufacturer_id),
            "family_code": "{}.{}".format(family_code_one, family_code_two),
            "model_number": "{}.{}".format(model_number_one, model_number_two),
            "version": "{}.{}.{}.{}".format(version_number_one, version_number_two, version_number_three, version_number_four)
        }

        sysex_end = readOneByte()
        if sysex_end != MIDI_SYSEX_END:
            raise MidiError('Received byte of 0x{:02X}, but expected MIDI_SYSEX_END (0x{:02X}) Aborting.'.format(sysex_end, MIDI_SYSEX_END))

        return identity

    # We need to wait until the arduino is ready to read...
    start = time.time()

    while time.time() - start < 3.0:
        if serialMidi.in_waiting > 0:
            identity = tryParsingSysexIdentityReply()
            if identity is not False:
                return identity



    serialMidi.write(bytearray([
        MIDI_SYSEX,
        MIDI_SYSEX_TYPE_NON_REALTIME,
        0x1,
        MIDI_SYSEX_GENERAL_INFORMATION, 
        MIDI_SYSEX_REQUEST_IDENTITY,
        MIDI_SYSEX_END
    ]))
    serialMidi.flush()
    print("[i] Sent request")

    while True:
        identity = tryParsingSysexIdentityReply()
        if identity is not False:
            return identity


def help():
    print ("""test-sysex-identity-request: <device>

Example: test-sysex-identity-request /dev/ttyUSB0""")


def string_to_int(string):
    integer = None
    try:
        integer = int(string)
    except ValueError:
        pass
    try:
        integer = int(string, base=16)
    except ValueError:
        pass

    if integer is None:
        raise Exception("Cannot parse " + string)
    return integer

def main():
    show_help = len(sys.argv) != 2
    
    parsed_argv = []
    if show_help is False:
        for i in range(2, len(sys.argv)):
            parsed_argv.append(string_to_int(sys.argv[i]))
        print(parsed_argv)

    if show_help is True:
        help()
        return
    device = sys.argv[1]
    identity = sysexIdentityRequest(device)


    print('Manufacturer: {}'.format(identity["manufacturer"]))
    print('Family code: {}'.format(identity["family_code"]))
    print('Model number: {}'.format(identity["model_number"]))
    print('Version: {}'.format(identity["version"]))

    

if __name__ == "__main__":
    main()


